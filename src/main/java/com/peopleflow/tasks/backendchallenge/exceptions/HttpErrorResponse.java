/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peopleflow.tasks.backendchallenge.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mohamedzaki
 */
public class HttpErrorResponse {
    
    @JsonProperty
    private Date timestamp;
    @JsonProperty
    private HttpStatus status;
    @JsonProperty
    private List<String> errors;
    @JsonProperty
    private String message;

    HttpErrorResponse(String message, HttpStatus httpStatus, List<String> errors) {
        this.timestamp = new Date();
        this.message = message;
        this.status = httpStatus;
        this.errors = errors;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
