package com.peopleflow.tasks.backendchallenge.models;

public enum EmployeeStates {
    ADDED("ADDED"),
    IN_CHECK("IN-CHECK"),
    ACTIVE("ACTIVE"),
    APPROVED("APPROVED");

    // declaring private variable for getting values
    private String state;

    // getter method
    public String getState()
    {
        return this.state;
    }

    // enum constructor - cannot be public or protected
    private EmployeeStates(String state)
    {
        this.state = state;
    }

    public static EmployeeStates fromString(String text) {
        for (EmployeeStates s : EmployeeStates.values()) {
            if (s.state.equalsIgnoreCase(text)) {
                return s;
            }
        }
        return null;
    }
}
