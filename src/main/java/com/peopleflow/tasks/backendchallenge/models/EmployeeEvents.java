package com.peopleflow.tasks.backendchallenge.models;

public enum EmployeeEvents {
    EMPLOYEE_SUBMIT,
    EMPLOYEE_GET_HIRED,
    EMPLOYEE_SELECTED,
    EMPLOYEE_START_WORKING
}
