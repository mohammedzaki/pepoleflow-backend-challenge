package com.peopleflow.tasks.backendchallenge.models;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author mohamedzaki
 */
public class EmploymentContract {

    @Min(value = 18000, message = "Age should not be less than 18")
    @Max(value = 40000, message = "Age should not be greater than 150")
    private double salary;

    @NotNull(message = "start date cannot be null")
    private Date startDate;

    @NotNull(message = "end date cannot be null")
    private Date endDate;

    public EmploymentContract() {

    }

    public EmploymentContract(double salary, Date startDate, Date endDate) {
        this.salary = salary;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "EmploymentContract{" +
                "salary=" + salary +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
