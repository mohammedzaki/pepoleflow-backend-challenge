/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peopleflow.tasks.backendchallenge.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author mohamedzaki
 */
@Document
public class Employee {

    @Id
    private String id;

    @NotNull(message = "Name cannot be null")
    private String name;

    @Min(value = 15, message = "Age should not be greater than 15")
    @Max(value = 80, message = "Age should not be less than 80")
    private int age;

    @Email(message = "Email should be valid")
    @Indexed(unique = true)
    private String email;

    private EmploymentContract contractInformation;

    private String currentEmploymentStatus;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

    public Employee() {
    }

    public Employee(String name, int age, EmploymentContract contractInformation, EmployeeStates currentEmploymentStatus) {
        this.name = name;
        this.age = age;
        this.contractInformation = contractInformation;
        this.currentEmploymentStatus = currentEmploymentStatus.getState();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public EmploymentContract getContractInformation() {
        return contractInformation;
    }

    public void setContractInformation(EmploymentContract contractInformation) {
        this.contractInformation = contractInformation;
    }

    public EmployeeStates getCurrentEmploymentStatus() {
        return EmployeeStates.fromString(currentEmploymentStatus);
    }

    public void setCurrentEmploymentStatus(EmployeeStates currentEmploymentStatus) {
        this.currentEmploymentStatus = currentEmploymentStatus.getState();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", email='" + email + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", deletedAt=" + deletedAt +
                ", contractInformation=" + contractInformation +
                ", currentEmploymentStatus='" + currentEmploymentStatus + '\'' +
                '}';
    }
}
