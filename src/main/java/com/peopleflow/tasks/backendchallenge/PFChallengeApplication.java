package com.peopleflow.tasks.backendchallenge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PFChallengeApplication {

    public final static Logger logger = LoggerFactory.getLogger(PFChallengeApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PFChallengeApplication.class, args);
    }

}
