package com.peopleflow.tasks.backendchallenge.statemachine;

import com.peopleflow.tasks.backendchallenge.models.EmployeeEvents;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import org.springframework.messaging.Message;
import org.springframework.statemachine.state.State;

public interface IEmployeeStateChangeListener {
    void onStateChange(State<EmployeeStates, EmployeeEvents> state, Message<EmployeeEvents> message);
}
