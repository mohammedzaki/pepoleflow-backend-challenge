package com.peopleflow.tasks.backendchallenge.statemachine;

import com.peopleflow.tasks.backendchallenge.models.EmployeeEvents;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.access.StateMachineAccess;
import org.springframework.statemachine.access.StateMachineFunction;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.statemachine.support.LifecycleObjectSupport;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class EmployeeStateHandler extends LifecycleObjectSupport {

    @Autowired
    private StateMachine<EmployeeStates, EmployeeEvents> stateMachine;

    private Set<IEmployeeStateChangeListener> listeners = new HashSet<>();

    @Override
    protected void onInit() throws Exception {
        stateMachine
                .getStateMachineAccessor()
                .doWithAllRegions(new StateMachineFunction<StateMachineAccess<EmployeeStates, EmployeeEvents>>() {
                    @Override
                    public void apply(StateMachineAccess<EmployeeStates, EmployeeEvents> function) {
                        function.addStateMachineInterceptor(new StateMachineInterceptorAdapter<EmployeeStates, EmployeeEvents>() {
                            @Override
                            public void preStateChange(State<EmployeeStates, EmployeeEvents> state, Message<EmployeeEvents> message, Transition<EmployeeStates, EmployeeEvents> transition, StateMachine<EmployeeStates, EmployeeEvents> stateMachine, StateMachine<EmployeeStates, EmployeeEvents> rootStateMachine) {
                                //super.preStateChange(state, message, transition, stateMachine, rootStateMachine);
                                listeners.forEach(listener -> listener.onStateChange(state, message));
                            }
                        });
                    }
                });
    }

    public void registerListener(IEmployeeStateChangeListener listener) {
        listeners.add(listener);
    }

    public void handleEvent(Message event, EmployeeStates sourceState) {
        stateMachine.stop();
        stateMachine
                .getStateMachineAccessor()
                .doWithAllRegions(access -> access.resetStateMachine(new DefaultStateMachineContext<EmployeeStates, EmployeeEvents>(sourceState, null, null, null)));
        stateMachine.start();
        stateMachine.sendEvent(event);
    }
}
