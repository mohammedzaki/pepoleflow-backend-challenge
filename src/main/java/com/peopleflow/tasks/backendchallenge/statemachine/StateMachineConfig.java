package com.peopleflow.tasks.backendchallenge.statemachine;

import com.peopleflow.tasks.backendchallenge.PFChallengeApplication;
import com.peopleflow.tasks.backendchallenge.listeners.EmployeeStateChanged;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import com.peopleflow.tasks.backendchallenge.models.EmployeeEvents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

import java.util.EnumSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Configuration
@EnableStateMachine
public class StateMachineConfig {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EmployeeStateHandler stateHandler;

    @Bean
    public StateMachine<EmployeeStates, EmployeeEvents> stateMachine(StateMachineListener<EmployeeStates, EmployeeEvents> listener) throws Exception {
        StateMachineBuilder.Builder<EmployeeStates, EmployeeEvents> builder = StateMachineBuilder.builder();

        builder.configureStates()
                .withStates()
                .initial(EmployeeStates.ADDED)
                .end(EmployeeStates.ACTIVE)
                .states(EnumSet.allOf(EmployeeStates.class));

        builder.configureTransitions()
                .withExternal()
                .source(EmployeeStates.ADDED).target(EmployeeStates.IN_CHECK)
                .event(EmployeeEvents.EMPLOYEE_SELECTED)

                .and()

                .withExternal()
                .source(EmployeeStates.IN_CHECK).target(EmployeeStates.APPROVED)
                .event(EmployeeEvents.EMPLOYEE_GET_HIRED)

                .and()

                .withExternal()
                .source(EmployeeStates.APPROVED).target(EmployeeStates.ACTIVE)
                .event(EmployeeEvents.EMPLOYEE_START_WORKING);

        StateMachine<EmployeeStates, EmployeeEvents> stateMachine = builder.build();
        // stateMachine.addStateListener(listener);
        stateHandler.registerListener(new EmployeeStateChanged());

        return stateMachine;
    }

    @Bean
    public StateMachineListener<EmployeeStates, EmployeeEvents> listener() {
        return new StateMachineListenerAdapter<EmployeeStates, EmployeeEvents>() {
            @Override
            public void stateChanged(State<EmployeeStates, EmployeeEvents> from, State<EmployeeStates, EmployeeEvents> to) {
                logger.info("Employee State changed to " + to.getId());
            }

            @Override
            public void extendedStateChanged(Object key, Object value) {
                super.extendedStateChanged(key, value);
            }
        };
    }
}
