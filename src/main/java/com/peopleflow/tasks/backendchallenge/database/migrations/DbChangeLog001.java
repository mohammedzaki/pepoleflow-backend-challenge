/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peopleflow.tasks.backendchallenge.database.migrations;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import com.peopleflow.tasks.backendchallenge.models.Employee;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;

import java.util.List;

/**
 *
 * @author mohamedzaki
 */
@ChangeLog(order = "001")
public class DbChangeLog001 {

    @ChangeSet(order = "001", id = "ChangeLog1", author = "root")
    public void ChangeLog1(MongockTemplate mongoTemplate) {
        List<Employee> emps = List.of(
                new Employee("Test", 18, null, EmployeeStates.ADDED)
        );
        mongoTemplate.insertAll(emps);
    }
}
