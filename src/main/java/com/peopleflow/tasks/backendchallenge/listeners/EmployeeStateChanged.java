/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peopleflow.tasks.backendchallenge.listeners;

import com.peopleflow.tasks.backendchallenge.models.Employee;
import com.peopleflow.tasks.backendchallenge.models.EmployeeEvents;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import com.peopleflow.tasks.backendchallenge.repositories.IEmployeeRepository;
import com.peopleflow.tasks.backendchallenge.statemachine.IEmployeeStateChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 *
 * @author mohamedzaki
 */
@Service
public class EmployeeStateChanged implements IEmployeeStateChangeListener {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onStateChange(State<EmployeeStates, EmployeeEvents> state, Message<EmployeeEvents> message) {
        String employeeId = message.getHeaders().get("emp-id", String.class);
        logger.info("Emp ID " + employeeId + ", And Current state is " + state.getId());
    }
}
