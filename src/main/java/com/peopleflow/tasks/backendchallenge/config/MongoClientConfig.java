package com.peopleflow.tasks.backendchallenge.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.SpringDataMongoV3Driver;
import com.github.cloudyrock.spring.v5.MongockSpring5;

import static java.util.Collections.singletonList;

/**
 * @author mohamedzaki
 */
@Configuration
@EnableMongoRepositories(basePackages = {"com.peopleflow.tasks.backendchallenge.repositories"})
public class MongoClientConfig extends AbstractMongoClientConfiguration {

    private static final Logger log = LoggerFactory.getLogger(MongoClientConfig.class);

    @Value("${spring.data.mongodb.host}")
    private String mongoHost;

    @Value("${spring.data.mongodb.security.enable}")
    private String mongoSecurity;

    @Value("${spring.data.mongodb.port}")
    private String mongoPort;

    @Value("${spring.data.mongodb.authentication-database}")
    private String mongoAuthenticationDB;

    @Value("${spring.data.mongodb.database}")
    private String mongoDB;

    @Value("${spring.data.mongodb.username}")
    private String mongoUser;

    @Value("${spring.data.mongodb.password}")
    private String mongoPassword;

    public String getMongoHost() {
        String host = System.getenv("MONGODB_HOST");
        return (host == null) ? mongoHost : host;
    }

    public String getMongoPort() {
        String port = System.getenv("MONGODB_PORT");
        return (port == null) ? mongoPort : port;
    }

    public boolean mongoSecurityEnabled() {
        String security = System.getenv("MONGODB_Security");
        return (security == null) ? Boolean.parseBoolean(mongoSecurity) : Boolean.parseBoolean(security);
    }

    public String getAuthenticationDB() {
        String authDB = System.getenv("MONGODB_AuthenticationDB");
        return (authDB == null) ? mongoAuthenticationDB : authDB;
    }

    public String getAuthUser() {
        String user = System.getenv("MONGODB_AuthUser");
        return (user == null) ? mongoUser : user;
    }

    public String getAuthPassword() {
        String password = System.getenv("MONGODB_AuthPassword");
        return (password == null) ? mongoPassword : password;
    }

    private String getMongoUriStringWithSecurity() {
        return "mongodb://" + getAuthUser() + ":" + getAuthPassword() + "@" + getMongoHost() + ":" + getMongoPort() +
                "/?authSource=" + getAuthenticationDB() + "&readPreference=primary&ssl=false";
    }

    private String getMongoUriStringWithoutSecurity() {
        return "mongodb://" + getMongoHost() + ":" + getMongoPort();
    }

    public String getMongoUriString() {
        if (mongoSecurityEnabled()) {
            return getMongoUriStringWithSecurity();
        } else {
            return getMongoUriStringWithoutSecurity();
        }
    }

    @Override
    public String getDatabaseName() {
        String myDB = System.getenv("MONGODB_Database");
        return (myDB == null) ? mongoDB : myDB;
    }

    @Override
    protected void configureClientSettings(MongoClientSettings.Builder builder) {
        ConnectionString conn = new ConnectionString(getMongoUriString());
        if (mongoSecurityEnabled()) {
            builder
                    .credential(MongoCredential.createCredential(getAuthUser(), getAuthenticationDB(), getAuthPassword().toCharArray()))
                    .applyToClusterSettings(settings -> {
                        settings.hosts(singletonList(new ServerAddress(getMongoHost(), Integer.parseInt(getMongoPort()))));
                    });
        } else {
            builder
                    .applyToClusterSettings(settings -> {
                        settings.hosts(singletonList(new ServerAddress(getMongoHost(), Integer.parseInt(getMongoPort()))));
                    });
        }
    }

    @Bean
    public MongockSpring5.MongockApplicationRunner mongockApplicationRunner(
            ApplicationContext springContext,
            MongoTemplate mongoTemplate,
            ApplicationEventPublisher eventPublisher) {

        return MongockSpring5.builder()
                .setDriver(SpringDataMongoV3Driver.withDefaultLock(mongoTemplate))
                .addChangeLogsScanPackage("com.peopleflow.tasks.backendchallenge.database.migrations")
                .setSpringContext(springContext)
                .setEventPublisher(eventPublisher)
                .setTrackIgnored(true)
                .buildApplicationRunner();
    }

    @Override
    protected boolean autoIndexCreation() {
        return true;
    }
}
