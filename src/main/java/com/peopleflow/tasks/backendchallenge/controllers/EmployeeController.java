package com.peopleflow.tasks.backendchallenge.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.peopleflow.tasks.backendchallenge.exceptions.ResourceNotFoundException;
import com.peopleflow.tasks.backendchallenge.models.Employee;
import com.peopleflow.tasks.backendchallenge.models.EmployeeEvents;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import com.peopleflow.tasks.backendchallenge.repositories.IEmployeeRepository;
import com.peopleflow.tasks.backendchallenge.statemachine.EmployeeStateHandler;
import org.springframework.messaging.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;

@RestController
@Controller
@RequestMapping("/api/v1/employees")
public class EmployeeController {

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Autowired
    private EmployeeStateHandler stateHandler;

    @Autowired
    private StateMachine<EmployeeStates, EmployeeEvents> stateMachine;

    @GetMapping
    List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @GetMapping("/{id}")
    Employee updateEmployeeStatus(
            @NotNull(message = "A required parameter employeeId") @PathVariable("id") String employeeId) {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (!employee.isPresent()) {
            throw new ResourceNotFoundException("Employee not found");
        }
        return employee.get();
    }

    @PostMapping
    Employee save(@Valid @RequestBody Employee employee) throws JsonProcessingException {
        employee.setCreatedAt(new Date());
        employee.setUpdatedAt(new Date());
        employee.setCurrentEmploymentStatus(EmployeeStates.ADDED);
        employee = employeeRepository.insert(employee);
        updateStateMachine(employee, EmployeeEvents.EMPLOYEE_SUBMIT, employee.getCurrentEmploymentStatus());
        return employee;
    }

    @PutMapping("/{id}")
    Employee update(@NotNull(message = "A required parameter employeeId") @PathVariable("id") int employeeId,
                    @RequestBody Employee employee) {
        return null;
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteEmployee(@NotNull(message = "A required parameter employeeId") @PathVariable("id") int employeeId) {
        return null;
    }

    @PutMapping("/{id}/set-employee-selected")
    Employee setEmployeeSelected(
            @NotNull(message = "A required parameter employeeId") @PathVariable("id") String employeeId) {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (!employee.isPresent()) {
            throw new ResourceNotFoundException("Employee not found");
        }
        Employee updateEmployee = employee.get();
        var prevState = updateEmployee.getCurrentEmploymentStatus();
        updateEmployee.setCurrentEmploymentStatus(EmployeeStates.IN_CHECK);
        employeeRepository.save(updateEmployee);
        updateStateMachine(updateEmployee, EmployeeEvents.EMPLOYEE_SELECTED, prevState);
        return updateEmployee;
    }

    @PutMapping("/{id}/employee-get-hired")
    Employee employeeGetHired(
            @NotNull(message = "A required parameter employeeId") @PathVariable("id") String employeeId) {

        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (!employee.isPresent()) {
            throw new ResourceNotFoundException("Employee not found");
        }

        Employee updateEmployee = employee.get();
        var prevState = updateEmployee.getCurrentEmploymentStatus();
        updateEmployee.setCurrentEmploymentStatus(EmployeeStates.APPROVED);
        employeeRepository.save(updateEmployee);
        updateStateMachine(updateEmployee, EmployeeEvents.EMPLOYEE_GET_HIRED, prevState);
        return updateEmployee;
    }

    void updateStateMachine(Employee emp, EmployeeEvents evt, EmployeeStates prevState) {
        Message msg = MessageBuilder
                .withPayload(evt)
                .setHeader("emp-id", emp.getId())
                .build();
        System.out.println(emp.getId());
        stateHandler.handleEvent(msg, prevState);
    }

}
