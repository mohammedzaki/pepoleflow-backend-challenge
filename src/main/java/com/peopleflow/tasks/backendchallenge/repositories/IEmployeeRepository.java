/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peopleflow.tasks.backendchallenge.repositories;

import com.peopleflow.tasks.backendchallenge.models.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author mohamedzaki
 */
public interface IEmployeeRepository extends MongoRepository<Employee, String> {

}
