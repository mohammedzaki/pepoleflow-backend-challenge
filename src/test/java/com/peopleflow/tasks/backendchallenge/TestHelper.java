package com.peopleflow.tasks.backendchallenge;

import com.peopleflow.tasks.backendchallenge.models.Employee;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import org.springframework.stereotype.Component;

import java.util.List;
import static java.util.Arrays.asList;

@Component
public class TestHelper {
    Employee getJone() {
        return new Employee("Jone Alex", 29, null, EmployeeStates.ADDED);
    }

    Employee getMohamed() {
        return new Employee("Mohamed Zaki", 33, null, EmployeeStates.IN_CHECK);
    }

    List<Employee> getEmployees() {
        return asList(getJone(), getMohamed());
    }
}
