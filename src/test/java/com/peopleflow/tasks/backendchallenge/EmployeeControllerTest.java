package com.peopleflow.tasks.backendchallenge;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.peopleflow.tasks.backendchallenge.controllers.EmployeeController;
import com.peopleflow.tasks.backendchallenge.models.Employee;
import com.peopleflow.tasks.backendchallenge.models.EmployeeStates;
import com.peopleflow.tasks.backendchallenge.repositories.IEmployeeRepository;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


//@DataMongoTest
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureDataMongo
@WebMvcTest
public class EmployeeControllerTest {

    private static final String CONNECTION_STRING = "mongodb://%s:%d";

    @LocalServerPort
    private int port;
    //    @Autowired
//    private TestRestTemplate rest;
    // @Autowired
    // private IEmployeeRepository employeeRepository;
//    @Autowired
//    private TestHelper testHelper;
    private String URL = "localhost:8083";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IEmployeeRepository employeeRepository;

    @MockBean
    private MongoTemplate mongoTemplate;


//    @Autowired
//    EmployeeControllerTest(MongoClient mongoClient) {
//        createPersonCollectionIfNotPresent(mongoClient);
//    }

//    @PostConstruct
//    void setUp() {
//
//    }

    @AfterEach
    void tearDown() {
        //employeeRepository.deleteAll();
        //mongodExecutable.stop();
    }

    @DisplayName("POST /employees with 1 employee")
    @Test
    void postEmployee() throws Exception {
        // GIVEN
        Employee emp1 = new Employee("Jone Alex", 29, null, EmployeeStates.ADDED);
        given(this.employeeRepository.insert(emp1));

        this.mvc.perform(
                MockMvcRequestBuilders
                        .get("/employees")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.employees").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.employees[*].employeeId").isNotEmpty());
        // WHEN
//        ResponseEntity<Employee> result = rest.postForEntity(URL + "/employees", testHelper.getJone(), Employee.class);
//        // THEN
//        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
//        Employee employeeResult = result.getBody();
//        assertThat(employeeResult.getId()).isNotNull();
//        assertThat(employeeResult).usingRecursiveComparison().ignoringFields("id", "createdAt").isEqualTo(testHelper.getJone());
    }

//    private void createPersonCollectionIfNotPresent(MongoClient mongoClient) {
//        // This is required because it is not possible to create a new collection within a multi-documents transaction.
//        // Some tests start by inserting 2 documents with a transaction.
//        MongoDatabase db = mongoClient.getDatabase("test");
//        if (!db.listCollectionNames().into(new ArrayList<>()).contains("employee"))
//            db.createCollection("employee");
//    }
}
