My Solution

To Run the app

- cd {project_path}
- `./gradlew build && mkdir -p build/dependency -x test`
- `(cd build/dependency; jar -xf ../libs/*.jar)`
- `docker-compose up --build --force-recreate --renew-anon-volumes`
- OR in one line
- `./gradlew build -x test && mkdir -p build/dependency && (cd build/dependency; jar -xf ../libs/*.jar) && docker-compose up --build --force-recreate --renew-anon-volumes`
- for OpenAPI spec.: https://localhost:9444/swagger-ui.html
- Also, I added a sample DevOps that deploy to Google Kubernetes Cluster







