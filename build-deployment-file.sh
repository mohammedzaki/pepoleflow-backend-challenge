#!/bin/bash

SERVICE_NAME="eyeadv-live-chat-backend-$CI_DEPLOYMENT_ENV_NAME"
MONGO_SERVICE_NAME="mongodb-$CI_DEPLOYMENT_ENV_NAME"
MONGODB_PORT=27017
BACKEND_URL=""
APP_DISK_PV_NAME="eyeadv-live-chat-pv-claim"

if [ "$CI_DEPLOYMENT_ENV_NAME" == "staging" ]; then
    BACKEND_URL="https://beta-test.ezz.eye-ltd.com/api/v1"
else
    BACKEND_URL="https://pro.ezz.eye-ltd.com/api/v1"
fi

cat > deployment.yml <<EOL
---
  apiVersion: storage.k8s.io/v1
  kind: StorageClass
  metadata:
    name: fast
    labels:
      app: $SERVICE_NAME
  provisioner: kubernetes.io/gce-pd
  parameters:
    type: pd-ssd
---
  apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: $APP_DISK_PV_NAME
    labels:
      app: $SERVICE_NAME
  spec:
    storageClassName: fast
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: 10Gi
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    name: $MONGO_SERVICE_NAME
  name: mongo-$CI_DEPLOYMENT_ENV_NAME-controller
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: $MONGO_SERVICE_NAME
    spec:
      containers:
      - name: $MONGO_SERVICE_NAME
        image: mongo:latest
        ports:
        - containerPort: $MONGODB_PORT
        volumeMounts:
        - name: mongo-persistent-storage
          mountPath: /data/db
      volumes:
      - name: mongo-persistent-storage
        gcePersistentDisk:
          pdName: my-data-disk-$CI_DEPLOYMENT_ENV_NAME
          fsType: ext4
---
apiVersion: v1
kind: Service
metadata:
  labels:
    name: $MONGO_SERVICE_NAME
  name: $MONGO_SERVICE_NAME
spec:
  ports:
  - port: $MONGODB_PORT
  selector:
    name: $MONGO_SERVICE_NAME
---
apiVersion: v1
kind: Service
metadata:
  name: $SERVICE_NAME
  labels:
    run: $SERVICE_NAME
spec:
  type: LoadBalancer
  ports:
  - port: 443
    targetPort: 9443
    protocol: TCP
    name: https
  selector:
    run: $SERVICE_NAME
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: $SERVICE_NAME
spec:
  replicas: 1
  template:
    metadata:
      labels:
        run: $SERVICE_NAME
    spec:
      volumes:
        - name: storage-app-volume
          persistentVolumeClaim:
            claimName: $APP_DISK_PV_NAME
      containers:
      - name: $SERVICE_NAME
        image: $CI_BUILD_IMAGE_VERSION
        imagePullPolicy: Always
        ports:
        - containerPort: 9443
        env:
        - name: MONGODB_HOST
          value: $MONGO_SERVICE_NAME
        - name: MONGODB_Security
          value: "false"
        - name: Ezz_Eldden_Url
          value: "$BACKEND_URL"
        - name: MONGODB_PORT
          value: "$MONGODB_PORT"
        volumeMounts:
          - name: storage-app-volume
            mountPath: /storage
      imagePullSecrets:
        - name: registry.gitlab.com  
EOL